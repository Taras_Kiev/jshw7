"use strict";

// Теоретичні питання

// 1 - Це якась будь яка функція в середені .
// 2 - Не зрозумів питання (str та number?)
// 3 - Посилальний тип – це “тип специфікації”. Ми не можемо явно використовувати його, але він використовується всередині мови.

// Завдання практичне


function filterBy(arr, type) {
    let result = arr.filter(function(item){
        return typeof(item) !== type;
    });
  
    console.log(result);
}

filterBy( ['hello', 'world', 23, '23', null], "string");
